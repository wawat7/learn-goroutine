package app_goroutine

import (
	"fmt"
	"strconv"
	"testing"
	"time"
)

func TestCreateChannel(t *testing.T) {
	channel := make(chan string)
	defer close(channel)

	go func() {
		time.Sleep(2*time.Second)
		channel <- "Wawat Prigala"
		fmt.Println("selesai mengirim data ke channel")
	}()

	data := <- channel
	fmt.Println(data)
	time.Sleep(5*time.Second)
}

func GiveMeResponse(channel chan string) {
	time.Sleep(2 * time.Second)
	channel <- "Wawat Prigala"
}

func TestChannelAsParameter(t *testing.T) {
	channel := make(chan string)
	defer close(channel)

	go GiveMeResponse(channel)

	data := <- channel
	fmt.Println(data)
	time.Sleep(5*time.Second)
}

//channel hanya untuk mengirim
func OnlyIn(channel chan<- string) {
	time.Sleep(2 * time.Second)
	channel <- "Wawat Prigala"
}

//channel hanya untuk menerima
func OnlyOut(channel <-chan string) {
	data := <- channel
	fmt.Println(data)
}

func TestInOutChannel(t *testing.T)  {
	channel := make(chan string)
	defer close(channel)

	go OnlyIn(channel)
	go OnlyOut(channel)

	time.Sleep(5*time.Second)
}

func TestBufferedChannel(t *testing.T) {
	channel := make(chan string, 3) //with buffer
	defer close(channel)

	go func() {
		channel <- "Wawat"
		channel <- "Prigala"
		channel <- "Ganteng"
	}()

	go func() {
		fmt.Println(<- channel)
		fmt.Println(<- channel)
		fmt.Println(<- channel)
	}()

	time.Sleep(2 * time.Second)
	fmt.Println("Selesai")
}

func TestRangeChannel(t *testing.T) {
	channel := make(chan string)

	go func() {
		for i := 0; i < 10; i++ {
			channel <- "Perulangan ke" + strconv.Itoa(i)
		}
		close(channel)
	}()

	for data := range channel {
		fmt.Println("menerima data ", data)
	}

	fmt.Println("selesai")
		
}

func TestSelectChannel(t *testing.T) {
	channel1 := make(chan string)
	channel2 := make(chan string)
	defer close(channel1)
	defer close(channel2)

	go GiveMeResponse(channel1)
	go GiveMeResponse(channel2)

	counter := 0
	for {
		select {
		case data := <- channel1:
			fmt.Println("data dari channel 1", data)
			counter++
		case data := <- channel2:
			fmt.Println("data dari channel 2", data)
			counter++
		default:
			fmt.Println("menunggu data")
		}
		if counter == 2 {
			break
		}
	}
}

/*
 - jika channel di buat lalu di masukkan data tp tidak ada yg menerima channel tersebut maka akan
terjadi error

- jika channel di buat lalu TIDAK di masukkan data ke channel tp ADA channel yg mengambil data
maka akan terjadi deadlock
 */
