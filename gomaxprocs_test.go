package app_goroutine

import (
	"fmt"
	"runtime"
	"testing"
)

func TestGetGomaxprocs(t *testing.T) {
	totalCPU := runtime.NumCPU()
	fmt.Println("total cpu",totalCPU)

	totalThread := runtime.GOMAXPROCS(-1)
	fmt.Println("total thread",totalThread)

	totalGoroutine := runtime.NumGoroutine()
	fmt.Println("total goroutine", totalGoroutine)

}
