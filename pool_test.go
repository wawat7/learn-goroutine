package app_goroutine

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

func TestPool(t *testing.T) {
	pool := sync.Pool{
		//default
		New: func() interface{} {
			return "New"
		},
	}

	pool.Put("Wawat")
	pool.Put("Prigala")
	pool.Put("Ganteng")

	for i := 0; i < 10; i++ {
		go func() {
			data := pool.Get()
			fmt.Println(data)
			time.Sleep(1 * time.Second)
			pool.Put(data)
		}()
	}

	time.Sleep(11 *time.Second)
	fmt.Println("selesai")
}
